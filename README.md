# Synopsis
"MyTime!" is a Chrome Extension that displays messages to motivate and inspire you.

# Version 1.0
This version currently loads a page with a message when a new tab is opened.

# Screenshot
![Screenshot](screenshots/Screenshot.PNG)

# Future Features To Be Implemented
- at a specific time all webpages will be be blocked with a message reminding you to stop using the internet.
- custom messages

# Motivation
The idea for this came from my desire to remind myself not to watch random YouTube videos late into the night. Also, I wanted to create a website blocker which was not as visually jarring as others I have used.
The page will have a simple, clean, and balanced look, and with hopefully with future updates be more customizable, allowing users to create their own messages.

# Installation
Download the files as a .zip (you will need to unzip the .zip file), or fork from repository.

- In Chrome Browser, go to Extensions
- check "Developer Mode"
- click on "Load unpacked extension..."
- select unzipped folder "MyTime!" (ensure that it is checked "Enabled" in Chrome Extensions page)
- Open a new tab
- Read message and be inspired!

# API Reference
Chrome Extension API

# License
Free to use, but it would be nice if you credited me :)