/**
 * Created by Sonia Yap
 * Date Created: 13 October 2017
 * Date Modified: 16 October 2017
**/

'use strict';

var messages = [
    "Effort always pays off.",
    "Luck is when preparation meets opportunity.",
    "Fear is the essence of things not achieved.",
    "Knowledge is power.",
    "Do or do not. There is no try."
];

var random = messages[Math.floor(Math.random() * messages.length)];
document.getElementById("message").textContent = random.toString();